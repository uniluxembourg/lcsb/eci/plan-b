;;; This is used to set emacs up before the first run.

(require 'org)
(require 'ob-tangle)

(org-babel-tangle-file "~/.emacs.d/eci-init.org" "~/.emacs.d/eci-init.el")
