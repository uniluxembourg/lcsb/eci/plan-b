#!/bin/sh

# Clear environment, source the profile containing the new shinyscreen
# and run it.

env -i HOME="$HOME"\
    USER="$USER"\
    DISPLAY=":50"\
    PATH=/usr/local/sbin/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin\
    bash -c "source  '$HOME'/.man-guix-prof/etc/profile
             '$HOME'/.man-guix-prof/bin/Rscript '$HOME'/.local/bin/run-ss.R"
