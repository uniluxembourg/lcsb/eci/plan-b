;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(require 'package)

(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
    (not (gnutls-available-p))))
    (proto (if no-ssl "http" "https")))
    ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
    (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
    ;;(add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
    (when (< emacs-major-version 24)
      ;; For important compatibility libraries like cl-lib
      (add-to-list 'package-archives '("gnu" . (concat proto "://elpa.gnu.org/packages/")))))

(require 'ob-tangle)

(defun my--tangle-byte-compile-org ()
 "Tangles .emacs.org and byte compiles ~/.emacs.d/"
   (interactive)
   (when (equal (buffer-name)
                (concat ".emacs.org"))
     (org-babel-tangle)
     (byte-recompile-directory (expand-file-name user-emacs-directory) 0)))


(add-hook 'after-save-hook #'my--tangle-byte-compile-org)
(add-hook 'kill-emacs-hook #'my--tangle-byte-compile-org)
;; (add-hook 'org-export-before-processing-hook #'org-update-all-dblocks)

(defvar my-dotemacs "~/.emacs.d/dotemacs.org.el")
(defvar my-custom "~/.emacs.d/custom.el")
(setq custom-file my-custom)
(if (file-exists-p my-dotemacs) (load my-dotemacs) nil)
(if (file-exists-p my-custom) (load my-custom) nil)
