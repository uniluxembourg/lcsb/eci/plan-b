(use-modules (guix packages)
	     (guix profiles)
	     (gnu packages)
	     (srfi srfi-1))



(specifications->manifest       
 '( ;; Utilities
   "recutils"
   "curl"
   "git"
   "git:send-email"
   "bash"
   "guile"
   "nano"
   "nss-certs"
   "glibc-locales"
   "gcc-toolchain"
   "gfortran-toolchain"
   "python"
   "perl"
   "htop"
   "ansible"
   "pigz"


   ;; Autotools
   "autoconf"
   "automake"
   "autobuild"
   "m4"

   ;; Perl
   "perl-yaml-libyaml"

   ;; Guile
   "guile-readline"))
