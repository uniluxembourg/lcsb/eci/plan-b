(use-modules (guix packages)
	     (guix profiles)
	     (gnu packages)
	     (srfi srfi-1))



(specifications->manifest       
 '( ;; Utilities
   "curl"
   "git"
   "git:send-email"
   "subversion"
   "bash"
   "nano"
   "nss-certs"
   "glibc-locales"
   "gcc-toolchain"
   "gfortran-toolchain"
   "rsync"
   "python"
   "perl"
   "htop"
   "pigz"


   ;; Autotools
   "autoconf"
   "automake"
   "autobuild"
   "m4"

   ;;Python
   "python-requests"
   "python-ruamel.yaml"
   "python-matplotlib"
   "python-numpy"
   "python-scipy"

   ;; Java
   "openjdk:out"
   "openjdk:jdk"
   ;; Perl
   "perl-yaml-libyaml"

   ;; Guile
   "guile-readline"

   ;; R
   "r"
   "r-r-utils"
   "r-curl"
   "r-ellipsis"
   "r-remoter"
   "r-sass"
   "r-bslib"
   "r-cachem"
   "r-future"
   "r-rmarkdown"
   "r-knitr"
   "r-roxygen2"
   "r-rcolorbrewer"
   "r-data-table"
   "r-gplots"
   "r-devtools"
   "r-usethis"
   "r-shiny"
   "r-shinyfiles"
   "r-shinydashboard"
   "r-flexdashboard"
   "r-handsontable"
   "r-shinyfiles"
   "r-cowplot"
   "r-withr"
   "r-massbank"
   "r-chemmass"
   "r-resolution"
   "r-msnbase"
   "r-pander"
   "r-rstatix"
   "r-shinyscreen"))
